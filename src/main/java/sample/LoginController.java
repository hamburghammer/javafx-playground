package sample;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class LoginController {

    @FXML
    private TextField username;
    @FXML
    private TextField password;

    @FXML
    public void login() {
        log.info("logging in...");
        log.info("username: {}", username.textProperty().get());
        log.info("password: {}", password.textProperty().get());
    }
}
