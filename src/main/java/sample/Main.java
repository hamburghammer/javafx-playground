package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        log.info(getClass().getResource("/login.fxml").toString());
        FXRouter.bind(primaryStage);
        FXRouter.when("login", "/login.fxml");
        FXRouter.goTo("login");
    }


    public static void main(String[] args) {
        launch(args);
    }
}
