package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class LoginControllerTest extends ApplicationTest {

    private Scene login;

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/login.fxml"));
        login = new Scene(root);
        stage.setScene(login);
        stage.show();
    }

    @Test
    public void shouldSetGetter() {

        clickOn("#username");
        write("foo");

        clickOn("#password");
        write("bar");

        clickOn(".button");

        assertEquals("foo", lookup("#username").<TextField>query().getText());
        assertEquals("bar", lookup("#password").<PasswordField>query().getText());
    }

}
